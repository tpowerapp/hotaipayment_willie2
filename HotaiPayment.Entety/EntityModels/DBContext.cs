﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HotaiPayment.Entity.EntityModels
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Dealer> Dealers { get; set; }
        public virtual DbSet<PerPaymentOrder> PerPaymentOrders { get; set; }
        public virtual DbSet<SalesOffice> SalesOffices { get; set; }
        public virtual DbSet<Test> Tests { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_Taiwan_Stroke_CI_AS");

            modelBuilder.Entity<Dealer>(entity =>
            {
                entity.HasKey(e => e.DealerCode);

                entity.ToTable("Dealer");

                entity.Property(e => e.DealerCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DealerName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PerPaymentOrder>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.ToTable("PerPayment_Order");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Order_Id");

                entity.Property(e => e.CreditCardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Credit_Card_Number");

                entity.Property(e => e.DealerId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Dealer_Id");

                entity.Property(e => e.MemberId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Member_Id");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Order_Date");

                entity.Property(e => e.OrderStatus).HasColumnName("Order_Status");

                entity.Property(e => e.SalesOfficeId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Sales_Office_Id");

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.PerPaymentOrders)
                    .HasForeignKey(d => d.DealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PerPayment_Order_Dealer");

                entity.HasOne(d => d.SalesOffice)
                    .WithMany(p => p.PerPaymentOrders)
                    .HasForeignKey(d => d.SalesOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PerPayment_Order_Sales_Office");
            });

            modelBuilder.Entity<SalesOffice>(entity =>
            {
                entity.HasKey(e => e.Socode)
                    .HasName("PK_Business_Office");

                entity.ToTable("Sales_Office");

                entity.Property(e => e.Socode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SOCode");

                entity.Property(e => e.Soname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("SOName");
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.ToTable("Test");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
