﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HotaiPayment.Entity.EntityModels
{
    public partial class Dealer
    {
        public Dealer()
        {
            PerPaymentOrders = new HashSet<PerPaymentOrder>();
        }

        public string DealerCode { get; set; }
        public string DealerName { get; set; }

        public virtual ICollection<PerPaymentOrder> PerPaymentOrders { get; set; }
    }
}
