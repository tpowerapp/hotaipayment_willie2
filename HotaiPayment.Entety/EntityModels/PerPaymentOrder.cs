﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HotaiPayment.Entity.EntityModels
{
    public partial class PerPaymentOrder
    {
        public string OrderId { get; set; }
        public int? Amount { get; set; }
        public string DealerId { get; set; }
        public string SalesOfficeId { get; set; }
        public string MemberId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderStatus { get; set; }
        public string CreditCardNumber { get; set; }

        public virtual Dealer Dealer { get; set; }
        public virtual SalesOffice SalesOffice { get; set; }
    }
}
