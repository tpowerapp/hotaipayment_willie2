﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HotaiPayment.Entity.EntityModels
{
    public partial class SalesOffice
    {
        public SalesOffice()
        {
            PerPaymentOrders = new HashSet<PerPaymentOrder>();
        }

        public string Socode { get; set; }
        public string Soname { get; set; }

        public virtual ICollection<PerPaymentOrder> PerPaymentOrders { get; set; }
    }
}
