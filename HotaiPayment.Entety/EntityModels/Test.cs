﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HotaiPayment.Entity.EntityModels
{
    public partial class Test
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
