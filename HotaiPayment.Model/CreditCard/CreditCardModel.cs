﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class CreditCardModel
    {
        /// <summary>
        /// 信用卡ID
        /// </summary>
        [JsonPropertyName("Id")]
        public int CardID { get; set; }
        /// <summary>
        /// 信用卡號
        /// </summary>
        [JsonPropertyName("CardNoMask")]
        public string CardNumber { get; set; }
        /// <summary>
        /// 發卡銀行
        /// </summary>
        [JsonPropertyName("BankDesc")]
        public string BankName { get; set; }
        /// <summary>
        /// 會員OneID
        /// </summary>
        [JsonPropertyName("MemberOneID")]
        public string OneID { get; set; }
        /// <summary>
        /// 是否有效 1:有效 0:無效
        /// </summary>
        [JsonPropertyName("IsAvailable")]
        public bool IsAvaible { get; set; }
        /// <summary>
        /// 因重複綁卡失效 1:重複失效 0:有效
        /// </summary>
        [JsonPropertyName("IsOverwrite")]
        public bool IsOverwrite { get; set; }
    }
}
