﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model.Enum
{
    public enum OrderStatusEnum
    {
        訂單建立 = 0,
        處理中 = 1,
        付款成功 =2,
        付款失敗 = 3
    }
}
