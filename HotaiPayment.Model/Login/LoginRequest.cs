﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class LoginRequest
    {
        /// <summary>
        /// 身分證字號
        /// </summary>
        [StringLength(10,MinimumLength = 10,ErrorMessage ="身份證字號長度錯誤")]
        public string IdentityCode { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
    }
}
