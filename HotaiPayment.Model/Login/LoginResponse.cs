﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class LoginResponse
    {
        public string AccessToken { get; set; }
        public string RefresToken { get; set; }
    }
}
