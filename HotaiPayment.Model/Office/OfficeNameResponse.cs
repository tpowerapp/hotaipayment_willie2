﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class OfficeNameResponse
    {
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public string SOCode { get; set; }
        public string SOName { get; set; }
    }
}
