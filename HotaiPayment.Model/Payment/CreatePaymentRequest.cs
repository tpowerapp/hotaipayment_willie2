﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class CreatePaymentRequest
    {
        /// <summary>
        /// 經銷商ID
        /// </summary>
        public string MID { get; set; }
        /// <summary>
        /// 營業所ID
        /// </summary>
        public string StoreID { get; set; }
    }
}
