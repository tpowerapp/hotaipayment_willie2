﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class CreditCardTXNRequest
    {
        /// <summary>
        /// 會員卡號流水號ID
        /// </summary>
        [JsonPropertyName("TokenID")]
        public int TokenID { get; set; }
        /// <summary>
        /// 訂單金額
        /// </summary>
        [JsonPropertyName("PurchAmt")]
        public int PurchAmt { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        [JsonPropertyName("Lidm")]
        public string Lidm { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        [JsonPropertyName("TxType")]
        public string TxType { get; set; }
        /// <summary>
        /// 是否自動轉入請款檔
        /// </summary>
        [JsonPropertyName("AutoCap")]
        public int AutoCap { get; set; }
        /// <summary>
        /// 綁定後跳轉網址
        /// </summary>
        [JsonPropertyName("RedirectURL")]
        public string RedirectURL { get; set; }

    }
}
