﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class GetTXNRequest
    {
        /// <summary>
        /// 和泰提供token
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 帳單編號
        /// </summary>
        public string PaymentID { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 信用卡ID
        /// </summary>
        public int CardID { get; set; }
    }
}
