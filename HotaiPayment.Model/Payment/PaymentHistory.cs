﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class PaymentHistory
    {
        /// <summary>
        /// 款項類型
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 付款編號
        /// </summary>
        public string PaymentID { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 付款卡號
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
        /// 付款日期
        /// </summary>
        public DateTime PaymentDate { get; set; }
    }
}
