﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class RequestModel
    {
        private string _body = "";
        [JsonPropertyName("Method")]
        public string Method { get; set; }
        [JsonPropertyName("API")]
        public string API { get; set; }
        [JsonPropertyName("Body")]
        public string Body { get => _body; set => _body = value; }
    }
}
