﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HotaiPayment.Model
{
    public class TestCTBC
    {
        [JsonPropertyName("reqjsonpwd")]
        public string reqjsonpwd { get; set; }
    }
}
