﻿using HotaiPayment.Entity.EntityModels;
using HotaiPayment.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class BaseService : IBaseService
    {
        public DBContext _db;

        public BaseService(DBContext db) { _db = db; }
    }
}
