﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class CreditCardService : ICreditCardService
    {
        public List<CreditCardModel> CheckCard(ResponseDataModel<List<CreditCardModel>> output)
        {
            var data = output.data;
            var result = new List<CreditCardModel>();

            foreach (var item in data)
            {
                if (item.IsAvaible.Equals(true) && item.IsOverwrite.Equals(false))
                {
                    result.Add(item);
                }
            }
            return result;

        }
    }
}
