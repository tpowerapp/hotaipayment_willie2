﻿using HotaiPayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service.Interface
{
    public interface ICreditCardService
    {
        public List<CreditCardModel> CheckCard(ResponseDataModel<List<CreditCardModel>> output);
    }
}
