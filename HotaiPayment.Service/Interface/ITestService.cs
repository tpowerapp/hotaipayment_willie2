﻿using HotaiPayment.Entity.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service.Interface
{
    public interface ITestService
    {
        public bool AddTest(string query);
        public Task<IEnumerable<Test>> GetTests();
        public bool UpdateTest(Test query);
        public bool DeleteTest(int id);
    }
}
