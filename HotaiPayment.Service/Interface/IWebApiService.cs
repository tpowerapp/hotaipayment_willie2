﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service.Interface
{
    public interface IWebApiService
    {
        Task<T> PostApiAsync<T, T2>(string clientAddress, Dictionary<string, string> headers, T2 input);
    }
}
