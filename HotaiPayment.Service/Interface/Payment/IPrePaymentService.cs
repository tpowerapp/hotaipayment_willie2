﻿using HotaiPayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service.Interface
{
    public interface IPrePaymentService
    {
        public Task<string> CreatePrepaymentAsync(CreatePaymentRequest query, string userID);

        public Task<string> GetNewPrepaymentIDAsync(CreatePaymentRequest query, string userID);
        public Task<string> GetPaymentTXNAsync(GetTXNRequest query);
    }
}
