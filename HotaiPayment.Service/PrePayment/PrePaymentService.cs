﻿using HotaiPayment.Entity.EntityModels;
using HotaiPayment.Model;
using HotaiPayment.Model.Enum;
using HotaiPayment.Service.Interface;
using HotaiPayment.ShareProvider;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class PrePaymentService : BaseService, IPrePaymentService
    {
        private AppID _appID { get; set; }
        private readonly IWebApiService _webApiService;
        private ApiAddress _apiAddress { get; set; }

        public PrePaymentService(DBContext db, 
                                 IOptions<AppID> appID,
                                 IWebApiService webApiService,
                                 IOptions<ApiAddress> apiAddress) : base(db) 
        {
            _appID = appID.Value;
            _webApiService = webApiService;
            _apiAddress = apiAddress.Value;
        }



        public async Task<string> CreatePrepaymentAsync(CreatePaymentRequest query, string userID)
        {
            var orderID = await GetNewPrepaymentIDAsync(query, userID);

            var data = new PerPaymentOrder()
            {
                OrderId = orderID,
                DealerId = query.MID,
                SalesOfficeId = query.StoreID,
                MemberId = userID,
                OrderDate = DateTime.Now,
                OrderStatus = (int)OrderStatusEnum.訂單建立
            };

            await _db.AddAsync(data);

            await _db.SaveChangesAsync();

            return data.OrderId;

        }

        public async Task<string> GetNewPrepaymentIDAsync(CreatePaymentRequest query, string userID)
        {
            var datas = _db.PerPaymentOrders
                            .Where(x => 
                                x.DealerId == query.MID 
                                && x.SalesOfficeId == query.StoreID 
                                && x.MemberId == userID );

            var num = await datas.CountAsync() + 1;

            var result = $"{query.StoreID}_{userID}_{num:0000}";

            return result;

        }

        public async Task<string> GetPaymentTXNAsync(GetTXNRequest query)
        {
            var credicardTXN = new CreditCardTXNRequest()
            {
                TokenID = query.CardID,
                PurchAmt = query.Amount,
                TxType = "0",
                Lidm = query.PaymentID,
                AutoCap = 1,
                //Todo: 需確認重新導向網址內容
                RedirectURL = "https://demo.3drens.com/creditcard/response"
            };

            var header = new Dictionary<string, string>()
            {
                {"token", query.Token},
                {"appid", _appID.ID}
            };

            var encode = AesDesHelper.DesEncrypt(JsonSerializer.Serialize(credicardTXN));

            var request = new RequestModel()
            {
                Method = "POST",
                API = "/creditcard/pay/json",
                Body = encode
            };

            try
            {
                var response = await _webApiService.PostApiAsync<ResponseDataModel<CreditCardTXNResponse>, RequestModel>(_apiAddress.Address, header, request);
                return response.data.reqjsonpwd;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }          
            
        }
    }
}
