﻿using HotaiPayment.Entity.EntityModels;
using HotaiPayment.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class TestService : BaseService, ITestService
    {
        static readonly HttpClient client = new HttpClient();

        public TestService(DBContext db) : base(db) { }
        public bool AddTest(string query)
        {
            var data = new Test() { Name = query };

            _db.Tests.Add(data);

            var result = _db.SaveChanges();


            return result != 0;
        }

        public bool DeleteTest(int id)
        {
            var data = _db.Tests.FirstOrDefault(x => x.Id == id);

            _db.Tests.Remove(data);

            var result = _db.SaveChanges();


            return result != 0;
        }

        public async Task<IEnumerable<Test>> GetTests()
        {
            var result = _db.Tests.AsEnumerable();

            await ProcessRepositories();

            return result;
        }

        public bool UpdateTest(Test query)
        {
            _db.Tests.Update(query);

            var result = _db.SaveChanges();


            return result != 0;
        }
        private static async Task ProcessRepositories()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

            var stringTask = client.GetStringAsync("https://api.github.com/orgs/dotnet/repos");

            var msg = await stringTask;
            Console.Write(msg);
        }
    }
}
