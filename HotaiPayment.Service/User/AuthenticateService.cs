﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using HotaiPayment.ShareProvider;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly AppSettings appSettings;
        public AuthenticateService(IOptionsSnapshot<AppSettings> settings) { appSettings = settings.Value; }
        public LoginResponse Authenticate(LoginRequest query)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(appSettings.Secret);
            var issuer = appSettings.Issuer;
            var expiresUtcDateTime = DateTime.Now.AddMinutes(30);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new[]
                    {
                        new Claim(ClaimTypes.Name, AesDesHelper.DesEncrypt(query.Name)),
                        new Claim("IdentityCode", AesDesHelper.DesEncrypt(query.IdentityCode)),
                    }),
                Issuer = issuer,
                Expires = expiresUtcDateTime,
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var accountToken = tokenHandler.WriteToken(token);
            var refresToken = RefreshHashHelper.GetHash(accountToken);

            return new LoginResponse()
            {
                AccessToken = accountToken,
                RefresToken = refresToken
            };

        }
    }
}
