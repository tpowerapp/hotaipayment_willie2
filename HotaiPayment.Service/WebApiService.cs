﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using HotaiPayment.ShareProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace HotaiPayment.Service
{
    public class WebApiService : IWebApiService
    {
        private readonly HttpClient client = new HttpClient();
        private static readonly JsonSerializerOptions _jsonSerializerOptions = new JsonSerializerOptions
        {
            IgnoreNullValues = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
        public async Task<T> PostApiAsync<T, T2>(string clientAddress, Dictionary<string, string> headers, T2 input)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            foreach (var item in headers)
            {
                client.DefaultRequestHeaders.Add(item.Key, item.Value);
            }

            var fooJSON = JsonSerializer.Serialize(input, _jsonSerializerOptions);

            using var fooContent = new StringContent(fooJSON, Encoding.UTF8, "application/json");

            using var httpResponse =
                await client.PostAsync(clientAddress, fooContent);
            var response = await httpResponse.Content.ReadAsStringAsync();
            var decode = AesDesHelper.DesDecrypt(response);
            var result = JsonSerializer.Deserialize<T>(decode);
            return result;
        }
    }
}
