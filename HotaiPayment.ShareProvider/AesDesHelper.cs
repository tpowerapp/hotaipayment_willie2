﻿using HotaiPayment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HotaiPayment.ShareProvider
{
    public static class AesDesHelper
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="source">要編碼的字串</param>
        /// <returns></returns>
        public static string DesEncrypt(string source)
        {
            var sourceBytes = Encoding.UTF8.GetBytes(source);
            var key = Convert.FromBase64String("4/e2gAqZgPLzyxntbox7LM+Mf+moMSghVz79945RnsE=");
            var IV = Convert.FromBase64String("upMvv61PB6QRbz7zurY/xQ==");
            var aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = key;
            aes.IV = IV;
            var transform = aes.CreateEncryptor();
            return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="source">要解碼的字串</param>
        /// <returns></returns>
        public static string DesDecrypt(string source)
        {
            var encryptBytes = Convert.FromBase64String(source);
            var key = Convert.FromBase64String("4/e2gAqZgPLzyxntbox7LM+Mf+moMSghVz79945RnsE=");
            var IV = Convert.FromBase64String("upMvv61PB6QRbz7zurY/xQ==");
            var aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = key;
            aes.IV = IV;
            var transform = aes.CreateDecryptor();
            return Encoding.UTF8.GetString(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
        }
    }
}
