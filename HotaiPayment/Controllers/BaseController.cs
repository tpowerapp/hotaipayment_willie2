﻿using HotaiPayment.ShareProvider;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotaiPayment.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        public string UserName =>
                 AesDesHelper.DesDecrypt(User.Identities.FirstOrDefault()?.Claims.FirstOrDefault()?.Value);

        public string UserIdentity =>
                 AesDesHelper.DesDecrypt(User.Identities.FirstOrDefault()?.Claims.FirstOrDefault(x => x.Type == "IdentityCode")?.Value);

        public string MaskedUserName => !string.IsNullOrEmpty(UserName)
                                            ? UserName.Length > 1
                                                ? UserName.Length < 3
                                                    ? $"{UserName.Substring(0, 1)}*"
                                                    : $"{UserName.Substring(0, 1)}*{UserName.Substring(2)}"
                                                : UserName
                                            : "";
        public string MaskedUserIdentity => !string.IsNullOrEmpty(UserIdentity)
                                                ? $"{UserIdentity.Substring(0, 3)}****{UserIdentity.Substring(7)}"
                                                : "";
    }
}
