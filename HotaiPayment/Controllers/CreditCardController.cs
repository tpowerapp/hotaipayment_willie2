﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotaiPayment.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditCardController : BaseController
    {
        private AppID _appID { get; set; }
        private ApiAddress _apiAddress { get; set; }
        private readonly IWebApiService _webApiService;
        private readonly ICreditCardService _creditCardService;

        public CreditCardController(IOptions<AppID> appID, IWebApiService webApiService, IOptions<ApiAddress> apiAddress, ICreditCardService creditCardService)
        {
            _appID = appID.Value;
            _webApiService = webApiService;
            _apiAddress = apiAddress.Value;
            _creditCardService = creditCardService;
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CreditCardModel>))]
        public async Task<IActionResult> GetCreditCards(TestModel token)
        {
            var header = new Dictionary<string, string>()
            {
                {"token", token.Token},
                {"appid", _appID.ID}
            };

            var request = new RequestModel()
            {
                Method = "GET",
                API = "/creditcard/card",
                Body = ""
            };
            var response = await _webApiService.PostApiAsync<ResponseDataModel<List<CreditCardModel>>, RequestModel>(_apiAddress.Address, header, request);

            var result = _creditCardService.CheckCard(response); //過濾是否重複綁卡和是否有效

            return Ok(result);
        }

        [HttpPost("AddCard/{accessToken}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RedirectModel))]
        public async Task<IActionResult> AddCard(string accessToken)
        {
            return Ok();
        }

        [HttpPost("AddCoBrandedCard/{accessToken}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RedirectModel))]
        public async Task<IActionResult> AddCoBrandedCard(string accessToken)
        {
            return Ok();
        }
    }
}
