﻿using HotaiPayment.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotaiPayment.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : BaseController
    {
        [HttpGet("{dealerCode}/{boCode}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OfficeNameResponse))]
        public async Task<IActionResult> GetOfficeName(string dealerCode, string boCode)
        {
            return Ok();
        }
    }
}
