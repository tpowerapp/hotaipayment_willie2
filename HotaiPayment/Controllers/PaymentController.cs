﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotaiPayment.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentController : BaseController
    {
        private readonly IPrePaymentService _prePaymentService;

        public PaymentController(IPrePaymentService prePaymentService)
        {
            _prePaymentService = prePaymentService;
        }

        /// <summary>
        /// 取得預付訂單ID
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost("PaymentID")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CreatePaymentID(CreatePaymentRequest query)
        {
            try
            {
                var result = await _prePaymentService.CreatePrepaymentAsync(query, UserIdentity);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost("History")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PaymentHistory>))]
        public async Task<IActionResult> GetPaymentHistory()
        {
            return Ok();
        }

        [HttpPost("IsPaidToday/{paymentType}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public async Task<IActionResult> IsPaidToday(int paymentType) 
        {
            return Ok();
        }

        /// <summary>
        /// 取得付款TXN
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreditCardTXNResponse))]
        [HttpPost("GetTXN")]
        public async Task<IActionResult> GetTXN(GetTXNRequest query)
        {
            var response = await _prePaymentService.GetPaymentTXNAsync(query);

            return Ok(response);
        }
    }
}
