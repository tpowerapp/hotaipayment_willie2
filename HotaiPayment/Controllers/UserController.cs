﻿using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotaiPayment.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : BaseController
    {
        private readonly IAuthenticateService _authenticateService;

        public UserController(IAuthenticateService authenticateService)
        {
            _authenticateService = authenticateService;
        }
        /// <summary>
        /// 登入取得驗證
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost("authenticate")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LoginResponse))]
        public async Task<IActionResult> Login(LoginRequest query)
        {
            var token = _authenticateService.Authenticate(query);
            return Ok(token);
        }

        /// <summary>
        /// 取得登入人遮蔽明細
        /// </summary>
        /// <returns></returns>
        [HttpGet("UserInfo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LoginRequest))]
        public async Task<IActionResult> GetUserInfo()
        {
            var result = new LoginRequest()
            {
                Name = MaskedUserName,
                IdentityCode = MaskedUserIdentity
            };

            return Ok(result);
        }
    }
}
