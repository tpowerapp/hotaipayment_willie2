﻿using HotaiPayment.Entity.EntityModels;
using HotaiPayment.Model;
using HotaiPayment.Service.Interface;
using HotaiPayment.ShareProvider;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace HotaiPayment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private AppID _appID { get; set; }
        private ApiAddress _apiAddress { get; set; }
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ITestService _testService;
        private readonly IWebApiService _webApiService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
                                         ITestService testService,
                                         IWebApiService webApiService,
                                         IOptions<AppID> appID,
                                         IOptions<ApiAddress> apiAddress)
        {
            _logger = logger;
            _testService = testService;
            _webApiService = webApiService;
            _appID = appID.Value;
            _apiAddress = apiAddress.Value;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost("Test")]
        public async Task<IActionResult> GetTest(TestModel token)
        {
            var result = await _testService.GetTests();

            //var data = new PostReqest() { data = "VdrNFUcCL1GYFbhyzAHc8MMv6rYeCeuYNYiX4l3Lo8fx+mAp2kNuYDpjXx44JTpy9ewToPccXmAFpL89KZXrFYvWq4BuSxM4lzoOJLa+fQbwZ1JlTMQVnQ2fmvdlerNTleWrhN66ICMHo1IICGoPjukhafK3Ed+2Iw8+s2B7A/GmHYfHXeSAt82OFjn52Evzv7+eZjhNZ3vEofTuK4LX+quyOc464AlSxDmJiTZnS3sJsypVKLtEeLuivvfPz+Z3FIpx2WZMR62g0EsRGCZaZkrQhnxZFFa5Ma9k66Qg74Wx4PGWeOGMpNfaDKGlIwSWu0qQsj0xJt9MJ257oZM/VK0b5+cZe5utxUfA4YKwwzOodvnOMx0Dey5+1Z95vzEHL5Ndip+Ekb8YkUXVJowfJzHC+TGU6LeRL+SqVuGujrW+35cMCJRHfr20ME17FKHQsAJUMvzQRg/W6i/MnfHqYbwbwL1cJPy4xo7M2UxjCgtJBuRKJWs732kmsSG0t/bfp+HqTiUJ7xt+fk26vngRHL9qJkpFyL1SDvYensixwxXZlOmgEIClzDg4vehY5m3t/NaymL2z1Igh2OgdCefMpv22KALYb2/62sO/izdQCagMREGw9E/o+7R7mmM/sipNh17AsEOVY8CE/gbxLvxbNPB+LHgayHY2fb3K8tTpBuLGScZZKZuZiH5DEajmY8zlAgVJfvnVbK8nA95JO6d9UOJVc148I2E1WZpizV0UFO3NM0EYTw9aFFUQu6w2eDRxZHytZ5JlHbrbbDnpX10kWtkbfcxSZI4CnW5j/tyrO1B+YYOjVfs7nBuKqnZkt8tD/s/GyNoIYoqZnt2GzbR/I3c6IpoYdGt0zgi7uzlqj0fn9hv5n0w5kFxLu8m7IKge4nVF7uHJ53KxTxaTfGuMp1TYNdmWnp0Bp0AEusvIk8s=" };


            //var testPost = await _webApiService.PostApiAsync<PostResponse, PostReqest>("https://payment-center-dev.azurewebsites.net/yoxi/decrypted", new Dictionary<string, string>(), data);

            var credicardTXN = new CreditCardTXNRequest()
            {
                TokenID = 306,
                PurchAmt = 10,
                TxType = "0",
                Lidm = "Test1234",
                AutoCap = 1,
                RedirectURL = "https://demo.3drens.com/creditcard/response"
            };

            var encode = AesDesHelper.DesEncrypt(JsonSerializer.Serialize(credicardTXN));

            var header = new Dictionary<string, string>()
            {
                {"token", token.Token},
                {"appid", _appID.ID}
            };

            var request = new RequestModel()
            {
                Method = "POST",
                API = "/creditcard/pay/json",
                Body = encode
            };

            var testResult = await _webApiService.PostApiAsync<ResponseDataModel<CreditCardTXNResponse>, RequestModel>(_apiAddress.Address, header, request);

            using WebClient webClient = new WebClient();

            NameValueCollection vals = new();
            vals.Add("reqjsonpwd", "{\"Request\":{\"Header\":{\"ServiceName\":\"PayJSON\",\"Version\":\"1.0\",\"MerchantID\":\"8220130020262\",\"RequestTime\":\"2021/06/16 17:00:25\"},\"Data\":{\"MAC\":\"99080C7B\",\"TXN\":\"52745969A7180AC8244A0A79849821B4EE39C4876DF08796CF2168DC69EA79F7FC0F89C805987AA68641E35882FB7295DD268760B700184C6D1B8B4E8CE2066D7869EA1C70AA31417DFE1221C5A4A9D18EF2B0E48BB28685D6473F90198C22BC60F63E92039B13961B3295D8B01061A732E63C54B8ACEE0591D49E0DA01373ED46D050DFF2C06DE30FCEFDAE9F84BCD5D1F9572926AD3B53A4078FB2A35582F2D05D49445B904F9CD59C75AC2B676C888DD5498D7B7272EFFB58A9EF2F9EEDC4BE26838996687C5331708017B7FF7C6A81C1C3CA204144799BFFE9ED118DB8D784DFAA5406BCD6ED437607D1F8593C45B5EF33864838B7B3671F997BBF841D0C3D780EE7EE7B7BE8497BE8C5724F2C65952D5DA4C47390901D059D1A776F3F44521126A81163DB207567D8DDEA8C04E7056211CC465DD4D64138C2B55593D7E05E5324E180BFCC65CDF2014715AF11CEBC868008409B959606EE1798B3144D599D7A22F310CA3E4A466026552A909DD01447283B3FB37C2236363AFA773341F40547A1B70EFED95C\"}}}");
            var test = webClient.UploadValues("https://testepos.ctbcbank.com:9443/mFastPay/TxnServlet", vals);

            var ctbc = new TestCTBC()
            {
                reqjsonpwd = "{\"Request\":{\"Header\":{\"ServiceName\":\"PayJSON\",\"Version\":\"1.0\",\"MerchantID\":\"8220130020262\",\"RequestTime\":\"2021/06/16 17:00:25\"},\"Data\":{\"MAC\":\"99080C7B\",\"TXN\":\"52745969A7180AC8244A0A79849821B4EE39C4876DF08796CF2168DC69EA79F7FC0F89C805987AA68641E35882FB7295DD268760B700184C6D1B8B4E8CE2066D7869EA1C70AA31417DFE1221C5A4A9D18EF2B0E48BB28685D6473F90198C22BC60F63E92039B13961B3295D8B01061A732E63C54B8ACEE0591D49E0DA01373ED46D050DFF2C06DE30FCEFDAE9F84BCD5D1F9572926AD3B53A4078FB2A35582F2D05D49445B904F9CD59C75AC2B676C888DD5498D7B7272EFFB58A9EF2F9EEDC4BE26838996687C5331708017B7FF7C6A81C1C3CA204144799BFFE9ED118DB8D784DFAA5406BCD6ED437607D1F8593C45B5EF33864838B7B3671F997BBF841D0C3D780EE7EE7B7BE8497BE8C5724F2C65952D5DA4C47390901D059D1A776F3F44521126A81163DB207567D8DDEA8C04E7056211CC465DD4D64138C2B55593D7E05E5324E180BFCC65CDF2014715AF11CEBC868008409B959606EE1798B3144D599D7A22F310CA3E4A466026552A909DD01447283B3FB37C2236363AFA773341F40547A1B70EFED95C\"}}}"
            };

            var testCTBC = await _webApiService.PostApiAsync<ResponseDataModel<CreditCardTXNResponse>, TestCTBC>("https://testepos.ctbcbank.com:9443/mFastPay/TxnServlet", new Dictionary<string, string>(), ctbc);

            return Ok(testResult);
        }

        [HttpPost("Test1")]
        public IActionResult AddTest(string name)
        {
            var result = _testService.AddTest(name);

            return Ok(result);
        }

    }
}
