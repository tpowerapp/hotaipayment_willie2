﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NJsonSchema.Generation;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace HotaiPayment.web.Infars
{
    public static class NSwagStartup
    {
        public static IServiceCollection AddNSwagSetting(this IServiceCollection services)
        {
            services.AddOpenApiDocument(
                config =>
                {
                    // 設定文件名稱
                    config.DocumentName = "v1";

                    // 設定文件或 API 版本資訊
                    config.Version = "1.0.0";

                    // 設定文件標題 (當顯示 Swagger/ReDoc UI 的時候會顯示在畫面上)
                    config.Title = "HotaiPayment";

                    // 設定文件簡要說明
                    config.Description = "HotaiPayment";

                    // 是否要顯示 API 呼叫範例
                    config.GenerateExamples = true;

                    // 顯示 enum 描述 
                    config.GenerateEnumMappingDescription = true;

                    var apiScheme = new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    };

                    config.AddSecurity("JWT Token", Enumerable.Empty<string>(), apiScheme);

                    // config.AddSecurity("API Token", Enumerable.Empty<string>(), apiScheme);

                    config.OperationProcessors.Add(
                        new AspNetCoreOperationSecurityScopeProcessor("JWT Token"));

                    config.DefaultResponseReferenceTypeNullHandling = ReferenceTypeNullHandling.Null;
                }
            );

            return services;
        }

        public static IApplicationBuilder UseNSwag(this IApplicationBuilder builder, IWebHostEnvironment env)
        {
            builder.UseOpenApi(
                config =>
                {
                    // 這裡的 Path 用來設定 OpenAPI 文件的路由 (網址路徑) (一定要以 / 斜線開頭)
                    config.Path = "/swagger/v1/swagger.json";

                    // 這裡的 DocumentName 必須跟 services.AddOpenApiDocument() 的時候設定的 DocumentName 一致！
                    config.DocumentName = "v1";
                }
            );

            builder.UseSwaggerUi3();

            builder.UseReDoc(
                config => // serve ReDoc UI
                {
                    config.Path = "/redoc";
                    config.DocumentPath = "/swagger/v1/swagger.json";
                });

            return builder;
        }
    }
}
