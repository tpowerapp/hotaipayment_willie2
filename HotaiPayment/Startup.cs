using HotaiPayment.Entity.EntityModels;
using HotaiPayment.Model;
using HotaiPayment.Service;
using HotaiPayment.Service.Interface;
using HotaiPayment.ShareProvider;
using HotaiPayment.web.Infars;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace HotaiPayment
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment webEnv)
        {
            Configuration = configuration;
            Env = webEnv;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Env { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddNSwagSetting();
            services.AddCors();

            var appSettingsSection = Configuration.GetSection("AppSettings");

            var appSettings = appSettingsSection.Get<AppSettings>();
            services.Configure<AppSettings>(appSettingsSection);
            services.Configure<AppID>(Configuration.GetSection("AppID"));
            services.Configure<ApiAddress>(Configuration.GetSection("ApiAddress"));
            var key = Encoding.UTF8.GetBytes(appSettings.Secret);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(
                        x =>
                        {
                            x.RequireHttpsMetadata = false;
                            x.SaveToken = true;
                            x.IncludeErrorDetails = true;
                            x.TokenValidationParameters = new TokenValidationParameters
                            {
                                NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                                RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role",
                                ValidateLifetime = true,
                                ValidateIssuerSigningKey = false,
                                IssuerSigningKey = new SymmetricSecurityKey(key),
                                ValidateIssuer = true,
                                ValidIssuer = Configuration.GetValue<string>("AppSettings:Issuer"),
                                ValidateAudience = false
                            };
                        });
            services.AddControllers(
                    options =>
                    {
                        options.Filters.Add(new ProducesAttribute("application/json"));
                        //for .Net 5 處理前端傳入的時間
                        options.ModelBinderProviders.RemoveType<DateTimeModelBinderProvider>();
                    })
                    //將應用程序中的所有控制器註冊到 DI 容器（如果尚未註冊），並將IControllerActivator註冊為ServiceBasedControllerActivator
                    .AddControllersAsServices()
                    .AddJsonOptions(
                        options =>
                        {
                            options.JsonSerializerOptions.IgnoreNullValues = true;
                            options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                            options.JsonSerializerOptions.MaxDepth = 0;
                            //前端傳入的時間轉為Local time
                            options.JsonSerializerOptions.Converters.Add(new DateTimeUtcConverter());

                        });

            var t = AesDesHelper.DesDecrypt(
                        Configuration.GetConnectionString("DefaultConnection"));

            services.AddDbContext<DBContext>(
                options => options.UseSqlServer(
                    AesDesHelper.DesDecrypt(
                        Configuration.GetConnectionString("DefaultConnection"))));

            services.AddScoped<IBaseService, BaseService>();
            services.AddScoped<ITestService, TestService>();
            services.AddScoped<IWebApiService, WebApiService>();
            services.AddScoped<IAuthenticateService, AuthenticateService>();
            services.AddScoped<IPrePaymentService, PrePaymentService>();
            services.AddScoped<ICreditCardService, CreditCardService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HotaiPayment v1"));
            }*/

            app.UseNSwag(env);

            app.UseHttpsRedirection();

            // global cors policy
            app.UseCors(
                x => x
                     .DisallowCredentials()
                     .AllowAnyOrigin()
                     .AllowAnyMethod()
                     .AllowAnyHeader()
            );

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
